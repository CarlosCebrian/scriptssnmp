#v1.0 snmp & snmpd installer

TMP_FILE="/tmp/installer_snmp"

clear
echo -e "\e[1m\n\n\n\n\n\n\n\n\n\n\n				SNMP INSTALLER v1.0"
echo -e "			AUTOR: CARLOS JESUS CEBRIAN SANCHEZ\n"
sleep 2

function msg_error () {
	echo -e "\e[31m\e[1m ---------------------------------------"
	echo -e " |   		ERROR		       |"
	echo -e " ---------------------------------------\e[0m"
}
function rm_tmp_file () {
	if [ -f $TMP_FILE ]; then
		echo -e "\e[1mBorrando Archivos temporales...\e[0m\n"
		rm -f $TMP_FILE
	fi
}

if [ $(whoami) != "root" ]; then
	msg_error
	echo -e "\e[1mERROR: El programa debe ejecutarse con permisos de superusuario"
	echo -e "\e[31mAbortando...\e[0m"
	exit 1
fi

if [ $(dpkg -l | grep "ii" | tr -s " " | cut -d " " -f 2 | grep -c "^dialog$") -eq 0 ]; then
	msg_error
	echo -e "\e[1mERROR: Se debe tener instalado el programa \"dialog\""
	echo -e "Instale el programa dialog e intente la instalacion de nuevo"
	echo -e "\e[31mAbortando...\e[0m"
	exit 1
fi

dialog --menu "Instalador de SNMP, elige una opcion: " 12 50 4 1 Instalar\ SNMP 2 Instalar\ SNMPD 3 Instalar\ SNMP\ \&\ SNMPD 4 Abortar\ Instalacion 2> $TMP_FILE
clear

function install_snmp () {
	echo -e "\e[1mInstalando SNMP\e[0m"
	if [ $(dpkg -l | grep "ii" | tr -s " " | cut -d " " -f 2 | grep -c "^snmp$") -eq 1 ]; then
		echo -e "\e[1mSNMP esta instalado\e[0m\n"
	else
		apt-get install -y snmp > /dev/null
		echo -e "\e[1mInstalacion de SNMP finalizada\e[0m\n"
	fi
	echo -e "\e[1mInstalando las MIBS\e[0m"
	apt-get update > /dev/null
	if [ $(apt-cache search "^snmp-mibs-downloader$" | wc -l ) -eq 0 ]; then
		msg_error
		echo -e "\e[1mERROR: No es posible descargar contenido non-free \"dialog\""
		echo -e "Verifica /apt/sources.list si el repositorio posee el flag non-free e intente de nuevo la instalacion"
		echo -e "\e[31mAbortando...\e[0m"
		exit 1
	else
		if [ $(dpkg -l | grep "ii" | tr -s " " | cut -d " " -f 2 | grep -c "^snmp-mibs-downloader$") -eq 1 ]; then
			echo -e "\e[1mLas MIBS estan instaladas\e[0m\n"
		else
			apt-get install -y snmp-mibs-downloader > /dev/null
			echo -e "\e[1mInstalacion de las MIBS finalizada\e[0m\n"
		fi
	fi
	rm_tmp_file
}

function install_snmpd () {
	echo -e "\e[1mInstalando SNMPD\e[0m"
	if [ $(dpkg -l | grep "ii" | tr -s " " | cut -d " " -f 2 | grep -c "^snmpd$") -eq 1 ]; then
		echo -e "\e[1mSNMPD esta instalado\e[0m\n"
	else
		apt-get install -y snmpd > /dev/null
		echo -e "\e[1mInstalacion de SNMPD finalizada\e[0m\n"
	fi
	rm_tmp_file
}

function install_twice () {
	echo -e "\e[1mHas elegido instalacion SNMP & SNMPD\e[0m"
	install_snmp
	install_snmpd
}

case $(cat $TMP_FILE) in
	1) install_snmp ;;
	2) install_snmpd ;;
	3) install_twice ;;
	*) echo -e "\e[1mHas abortado el programa\e[0m" & rm_tmp_file & exit 0 ;;
esac

sleep 3

function configurar_snmp () {
	if [ $(dpkg -l | grep "ii" | tr -s " " | cut -d " " -f 2 | grep -c "^snmp$") -eq 1 ]; then
		echo -e "\e[1mConfigurando SNMP\e[0m"
		echo -e "\e[1mDescargando las MIBS...\e[0m"
		download-mibs > /dev/null
		echo -e "\e[1mMIBS Descargadas\e[0m\n"
		
		echo -e "\e[1mAutoconfigurando snmp.conf ...\e[0m"
		if [ $(cat /etc/snmp/snmp.conf | grep -ce "^#mibs :") -eq 0 ]; then
			cat /etc/snmp/snmp.conf | sed -e "s/mibs :/#mibs :/g" > $TMP_FILE
			cp -f $TMP_FILE /etc/snmp/snmp.conf
		fi
		echo -e "\e[1mConfiguracion de SNMP finalizada\e[0m\n"
	fi
	rm_tmp_file
}

function configurar_snmpd () {
	if [ $(dpkg -l | grep "ii" | tr -s " " | cut -d " " -f 2 | grep -c "^snmpd$") -eq 1 ]; then
		echo -e "\e[1mConfigurando SNMPD\e[0m"
		echo -e "\e[1mAutoconfigurando snmpd.conf ...\e[0m"
		if [ $(cat /etc/snmp/snmpd.conf | grep -ce "^#agentAddress  udp:127") -eq 0 ]; then
			cat /etc/snmp/snmpd.conf | sed -e "s/agentAddress  udp:127/#agentAddress  udp:127/g" > $TMP_FILE
			cp -f $TMP_FILE /etc/snmp/snmpd.conf
		fi
		if [ $(cat /etc/snmp/snmpd.conf | grep -ce "^#agentAddress udp:161") -eq 1 ]; then
			cat /etc/snmp/snmpd.conf | sed -e "s/#agentAddress udp:161/agentAddress udp:161/g" > $TMP_FILE
			cp -f $TMP_FILE /etc/snmp/snmpd.conf
		fi
		echo -e "\e[1mConfiguracion de SNMPD finalizada\e[0m\n"
	fi
	rm_tmp_file
}
if dialog --title "Configuracion SNMP & SNMPD" --yesno ¿Quieres\ configurar\ automaticamente\ snmp\ y\ snmpd? 6 50 ; then
	clear
	configurar_snmp
	configurar_snmpd
else
	clear
	echo "no"
fi
echo -e "\n\e[1mInstalacion finalizada... Saliendo del programa\e[0m\n"
exit 0
